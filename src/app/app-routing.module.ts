import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { QuizListComponent } from './core/quiz-list/quiz-list.component';
import { QuizComponent } from './core/quiz/quiz.component';
import { LoginComponent } from './core/login/login.component';
import { OceanResultComponent } from './core/ocean-result/ocean-result.component';


const routes: Routes = [
  {path: '', component: LoginComponent},
  {path: 'list', component: QuizListComponent},
  {path: 'quiz/:id', component: QuizComponent},
  {path: 'ocean/:id', component: OceanResultComponent},
  {path: '**', redirectTo: ''}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

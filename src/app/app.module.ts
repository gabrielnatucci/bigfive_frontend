import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { HeaderComponent } from './core/header/header.component';
import { QuizComponent } from './core/quiz/quiz.component';
import { FooterComponent } from './core/footer/footer.component';

import { HttpClientModule } from '@angular/common/http';
import { JwPaginationComponent } from './core/pagination/pagination.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgxTimerModule } from 'ngx-timer';
import { QuizListComponent } from './core/quiz-list/quiz-list.component';
import { LoginComponent } from './core/login/login.component';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { OceanResultComponent } from './core/ocean-result/ocean-result.component';




@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    QuizComponent,
    FooterComponent,
    JwPaginationComponent,
    QuizListComponent,
    LoginComponent,
    OceanResultComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgxTimerModule,
    SweetAlert2Module.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

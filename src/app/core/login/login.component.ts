import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent  {
  loginForm: FormGroup;

  constructor(private router: Router) {
    this.loginForm = this.createFormGroup();
  }

  createFormGroup(){
    return new FormGroup({
      userCode: new FormControl()
    });
  }

  onSubmit(){
    if(this.loginForm.value.userCode !== null){
      this.router.navigate(['/list', {userCode: this.loginForm.value.userCode}]);
    }
  }

  createNewCode(){
    console.log("create")
    this.router.navigate(['/list']);
  }

}

import { Component, OnInit } from '@angular/core';
import { QuizService } from 'src/app/services/quiz.service';
import { CountupTimerService } from 'ngx-timer';
import { ActivatedRoute, Router } from '@angular/router';
import { AnswerService } from 'src/app/services/answer.service';
import { Location } from '@angular/common';
import Swal from 'sweetalert2';
import { AnyTxtRecord } from 'dns';


@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.scss']
})
export class QuizComponent implements OnInit {
  quizId: any;
  quizzes: any;
  questionOnPage: Array<any>;
  answers = [];
  totalOfQuestions = 0;
  userId: any;
  formStats: any;

  constructor(private quizServices: QuizService, private countupTimerService: CountupTimerService,
    private route: ActivatedRoute, private answerService: AnswerService, private router: Router, private _location: Location) {
    this.route.queryParams.subscribe(params => {
      console.log(params);
      this.userId = params['user_id'];
      if(params.emotions !== undefined) {
        this.formStats = {stats: { emotions: params.emotions.split(','), game: params.game, user: params.user_id }};
        quizServices.postGameStats(this.formStats).subscribe(res => {
          console.log(res);
        });
      }
      
      if (this.userId === undefined || this.userId === '') {
        this.router.navigate(['']);
      }
    });

    this.route.params.subscribe(params => {
      console.log(params);
      this.quizId = params.id;
    });
  }

  ngOnInit() {
    this.countupTimerService.startTimer();
    this.quizServices.getQuiz(this.quizId, this.userId).subscribe(res => {
      this.quizzes = res.quiz;
      var obj = this.quizzes.questions[0].question_type.options;
      this.totalOfQuestions = this.quizzes.questions.length;
    });
  }

  ngOnDestroy() {
    this.countupTimerService.stopTimer();
  }

  onChangePage(questionOnPage: Array<any>) {
    localStorage.setItem("answers", JSON.stringify(this.answers));
    this.questionOnPage = questionOnPage;
  }

  addAnswser(question_id, value) {
    let alreadyExists = this.answers.filter(answer => answer.question === question_id);
    if (alreadyExists.length === 0) {
      let answer = { question: question_id, value: value };
      this.answers.push(answer);
    } else {
      let id = this.answers.findIndex(answer => answer.question === question_id);
      let answer = { question: question_id, value: value };
      this.answers[id] = answer;
    }
  }

  addTextAnswer(question_id, value) {
    let alreadyExists = this.answers.filter(answer => answer.question === question_id);
    if (alreadyExists.length === 0) {
      let answer = { question: question_id, text: value };
      this.answers.push(answer);
    } else {
      let id = this.answers.findIndex(answer => answer.question === question_id);
      let answer = { question: question_id, text: value };
      this.answers[id] = answer;
    }
  }

  checkTextValue(question_id) {
    let id = this.answers.findIndex(answer => answer.question === question_id);
    if (id !== -1) {
      return this.answers[id].text;
    }
  }

  checkValue(question_id, value) {
    let id = this.answers.findIndex(answer => answer.question === question_id);
    if (id !== -1) {
      if (this.answers[id].value === value) {
        return true;
      }
    } else {
      return false;
    }
  }

  saveAnswer() {
    let answers = { answers: { quiz: this.quizId, answers: this.answers, user: this.userId } };
    this.answerService.postAnswer(answers, this.userId).subscribe(res => {
      Swal.fire('Sucesso!', 'Formulário preenchido com sucesso!', 'success').then(res => {
        if(this.quizzes.phase === "PRE"){
          window.location.href = this.quizzes.post_page;
        } else {
          this.router.navigate(['/list', { user_id: this.userId }]);
        }
      });
    });
  }

  getObjectLength(object) {
    return Object.keys(object).length
  }

  getKeys(object) {
    return Object.keys(object).map(function (key) { return object[key]; });
  }

  getSize(object) {
    if (Object.entries(object).length === 0) {
      return true;
    } else {
      return false;
    }
  }

  goBack() {
    this._location.back();
  }

}

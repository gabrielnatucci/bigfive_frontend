import { Component, Input, Output, EventEmitter, OnInit, OnChanges, SimpleChanges } from '@angular/core';
declare const require: any;
import paginate = require('jw-paginate');

@Component({
  moduleId: module.id,
  selector: 'jw-pagination',
  template: `
  <nav class="pagination is-rounded is-centered" role="navigation" aria-label="pagination">
  <a (click)="setPage(1)" class="pagination-previous" [attr.disabled] = "pager.currentPage === 1 ? 'disabled' : null">First</a>
  <a (click)="setPage(pager.currentPage - 1)" [attr.disabled] = "pager.currentPage === 1 ? 'disabled' : null" class="pagination-previous" >Previous</a>

  <a (click)="setPage(pager.currentPage + 1)" [attr.disabled] = "pager.currentPage === pager.totalPages ? 'disabled' : null" class="pagination-next">Next page</a>
  <a (click)="setPage(pager.totalPages)" [attr.disabled] = "pager.currentPage === pager.totalPages ? 'disabled' : null" class="pagination-next">Last</a>

  <ul *ngIf="pager.pages && pager.pages.length" class="pagination-list">
    <li (click)="setPage(page)" *ngFor="let page of pager.pages" [ngClass]="{'is-current':pager.currentPage === page}" class="pagination-link">
      {{page}}
    </li>
  </ul>
  </nav>`
})

export class JwPaginationComponent implements OnInit, OnChanges {
  @Input() items: Array<any>;
  @Output() changePage = new EventEmitter<any>(true);
  @Input() initialPage = 1;
  @Input() pageSize = 10;
  @Input() maxPages = 10;

  pager: any = {};

  ngOnInit() {
    // set page if items array isn't empty
    if (this.items && this.items.length) {
      this.setPage(this.initialPage);
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    // reset page if items array has changed
    if (changes.items.currentValue !== changes.items.previousValue) {
      this.setPage(this.initialPage);
    }
  }

  public setPage(page: number) {
    // get new pager object for specified page
    this.pager = paginate(this.items.length, page, this.pageSize, this.maxPages);

    // get new page of items from items array
    var pageOfItems = this.items.slice(this.pager.startIndex, this.pager.endIndex + 1);

    // call change page function in parent component
    this.changePage.emit(pageOfItems);
  }
}
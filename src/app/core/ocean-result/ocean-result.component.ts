import { Component, OnInit, ElementRef, ViewChild, NgZone } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { OceanService } from 'src/app/services/ocean.service';
import { Location } from '@angular/common';

import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";

am4core.useTheme(am4themes_animated);

@Component({
  selector: 'app-ocean-result',
  templateUrl: './ocean-result.component.html',
  styleUrls: ['./ocean-result.component.scss']
})
export class OceanResultComponent {
  userId: any;
  oceanResult: any;
  chart: any;
  @ViewChild('chartElement', { static: true }) chartElement: ElementRef<HTMLElement>;


  constructor(private oceanService: OceanService, private route: ActivatedRoute, private router: Router,
    private zone: NgZone, private _location: Location) {
    this.route.params.subscribe(params => {
      console.log(params);
      this.userId = params.id;
    });
  }

  ngAfterViewInit() {
    this.oceanService.getOceanResult(this.userId).subscribe(res => {
      console.log(res);
      this.oceanResult = res;
      Object.keys(this.oceanResult).forEach(key => {
        this.zone.runOutsideAngular(() => {
          this.loadData(key);
        });
      });
    });
  }

  loadData(key) {
    console.log(key);
    console.log(this.oceanResult[key]);

    var chart = am4core.create(key.toLowerCase(), am4charts.GaugeChart);
    chart.innerRadius = -15;

    var axis = chart.xAxes.push(new am4charts.ValueAxis() as any);
    axis.min = this.oceanResult[key].min_score;
    axis.max = this.oceanResult[key].max_score;
    axis.strictMinMax = true;

    var colorSet = new am4core.ColorSet();

    var gradient = new am4core.LinearGradient();
    gradient.stops.push({ color: am4core.color("red") })
    gradient.stops.push({ color: am4core.color("yellow") })
    gradient.stops.push({ color: am4core.color("green") })

    axis.renderer.line.stroke = gradient;
    axis.renderer.line.strokeWidth = 30;
    axis.renderer.line.strokeOpacity = 1;

    axis.renderer.grid.template.disabled = true;

    var hand = chart.hands.push(new am4charts.ClockHand());
    hand.radius = am4core.percent(100);
    console.log(Math.random() * 100)
    hand.value = this.oceanResult[key].user_score;

    this.chart = chart;
  }

  goBack() {
    this._location.back();
  }

}

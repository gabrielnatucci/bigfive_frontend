import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OceanResultComponent } from './ocean-result.component';

describe('OceanResultComponent', () => {
  let component: OceanResultComponent;
  let fixture: ComponentFixture<OceanResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OceanResultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OceanResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

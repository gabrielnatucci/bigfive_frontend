import { Component, OnInit, AfterViewInit } from '@angular/core';
import { QuizService } from 'src/app/services/quiz.service';
import { ActivatedRoute, Router } from '@angular/router';
import { EmailService } from 'src/app/services/email.service';
import { FormGroup, FormControl } from '@angular/forms';
import Swal from 'sweetalert2';
import { Location } from '@angular/common';

@Component({
  selector: 'app-quiz-list',
  templateUrl: './quiz-list.component.html',
  styleUrls: ['./quiz-list.component.scss']
})
export class QuizListComponent {
  quizList: any;
  answerList: any;
  userId: string;
  emailForm: FormGroup;

  constructor(private quizService: QuizService, private activeRoute: ActivatedRoute, private router: Router,
    private mailService: EmailService, private _location: Location) {
    this.emailForm = this.createFormGroup();

    this.activeRoute.params.subscribe(params => {
      this.userId = params.user_id;
      this.fillQuizzes();
    });
  }

  createFormGroup() {
    return new FormGroup({
      email: new FormControl()
    });
  }

  fillQuizzes() {
    if (this.userId == null) {
      this.quizService.getQuizList().subscribe(res => {
        console.log(res);
        this.userId = res.user_id;
        this.quizList = res.quizzes;
        this.answerList = res.answers;
      });
    } else {
      this.quizService.getQuizListOfUser(this.userId).subscribe(res => {
        console.log(res);
        this.quizList = res.quizzes;
        this.answerList = res.answers;
        console.log(this.answerList);
      })
    }
  }

  oceanResult(quiz) {
    if (quiz.quiz_name === "Teste de Personalidade") {
      this.router.navigate(['/ocean', quiz.user_id], { state: { userId: this.userId } });
    }
  }

  navigateToQuiz(quizId, quiz) {
    const oceanBlock = this.quizList.filter(quiz => {
      if (quiz.title !== "Teste de Personalidade") {
        return quiz;
      }
    });
    if (quiz.title === "Teste de Personalidade" && oceanBlock.length > 0) {
      Swal.fire('Atenção!', 'Você deve completar todos os outros questionários antes de iniciar o teste de personalidade!', 'error');
    } else {
      this.router.navigate(['/quiz', quizId], { queryParams: { user_id: this.userId }});
    }
  }

  sendMail() {
    this.mailService.sendMail(this.emailForm.value, this.userId).subscribe(res => {
      Swal.fire('Sucesso!', 'Enviamos o alerta ao seu e-mail!', 'success');
    });
  }

  goBack() {
    this._location.back();
  }
}

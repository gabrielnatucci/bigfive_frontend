import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class QuizService {
  endpoint = environment.apiUrl;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor(private http: HttpClient) { }

  private extractData(res: Response) {
    let body = res;
    return body || {};
  }

  private handleError<T> (result?: T) {
    throw result;
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }

  getQuizListOfUser(userId):  Observable<any>{
    return this.http.get(this.endpoint + 'quizzes/' + userId + '?phase=PRE').pipe(
      catchError(this.handleError),
      map(this.extractData));
  }

  getQuizList(): Observable<any> {
    return this.http.get(this.endpoint + 'quizzes?phase=PRE').pipe(
      catchError(this.handleError),
      map(this.extractData));
  }

  getQuiz(id, userId): Observable<any> {
    return this.http.get(this.endpoint + 'quiz/' + id + '?user_id=' + userId).pipe(
      catchError(this.handleError),
      map(this.extractData));
  }

  postGameStats(stats): Observable<any> {
    return this.http.post(this.endpoint + 'game_stats', stats).pipe(
      catchError(this.handleError),
      map(this.extractData));
  }
}
